const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = env => {

    // Environment variables from script command
    const env_minify = env.minify;
    let minimize = false;

    // Conditional minification
    if(env_minify === 'true') {
        minimize = true;
    }

    return merge(common, {
        mode: 'production',
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        "postcss-loader",
                        "sass-loader",
                        "import-glob-loader"
                    ]
                }
            ]
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: "[name].css",
                chunkFilename: "[name].css",
            }),
            new CleanWebpackPlugin(['dist'],{})
        ],
        optimization: {
            minimize: minimize,
        }
    });

}
