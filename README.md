# Environment Configuration Panel #

[Epic for Environment Configuration Panel witht detailed list of tasks can be found here](https://jira.squiz.net/browse/CIP-360)

# Readme #
Currently does the following:

- Sass compilation + Post CSS autoprefixer
- Babel ES6 conversion
- ESLint
- Hot-reload HTML, CSS and JS

[Read the wiki for more information](https://gitlab.squiz.net/boilerplate/webpack-boilerplate/wikis/home) about installation, example projects, help and contributing.

## Installation

Read the [installation guide](https://gitlab.squiz.net/boilerplate/webpack-boilerplate/wikis/Installation)

